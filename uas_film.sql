-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2020 at 02:15 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uas_film`
--

-- --------------------------------------------------------

--
-- Table structure for table `film_baru`
--

CREATE TABLE `film_baru` (
  `id_film` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `id_genre` int(11) NOT NULL,
  `sinopsis` varchar(700) DEFAULT NULL,
  `foto` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `film_baru`
--

INSERT INTO `film_baru` (`id_film`, `judul`, `id_genre`, `sinopsis`, `foto`) VALUES
(0, 'The Boss Baby', 5, 'Kisahnya berawal dari Tim Templeton (Miles Christopher Bakshi), bocah berusia 7 tahun yang merasa kehidupannya sangat sempurna. Sayangnya kebahagiaan Tim tidak berlangsung lama karena datangnya anggota baru, bayi laki-laki yang sangat menggemaskan bernama Theodore alias Boss Baby (Alec Baldwin). Baby', 'baby.jpg'),
(2, 'Konosuba', 6, 'Menceritakan tentang Umaru yang memiliki sifat ganda', 'DC20200518065152.jpg'),
(3, 'Train to Busan 2', 1, 'Teror zombie masih berlanjut', 'DC20200513124639.jpg'),
(9, 'Parasite', 1, 'Menceritakan tentang keluarga yang miskin ingin merasakan menjadi orang kaya', 'DC20200518065137.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE `genre` (
  `id_genre` int(11) NOT NULL,
  `genre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`id_genre`, `genre`) VALUES
(1, 'Horor'),
(2, 'Komedi'),
(3, 'Action'),
(4, 'Romance'),
(5, 'Family'),
(6, 'Slice of Live');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `jam` time DEFAULT NULL,
  `id_genre` int(11) NOT NULL,
  `foto` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `judul`, `tanggal`, `jam`, `id_genre`, `foto`) VALUES
(1, 'Habibie Ainun 3', '2020-05-15', '14:40:00', 4, 'DC20200518065030.jpg'),
(2, '1917', '2020-05-17', '18:20:00', 3, 'DC20200518062631.jpg'),
(5, 'The New Muntans', '2020-12-03', '12:25:00', 1, 'DC20200518065015.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `film_baru`
--
ALTER TABLE `film_baru`
  ADD PRIMARY KEY (`id_film`),
  ADD KEY `id_genre` (`id_genre`);

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id_genre`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `film_baru`
--
ALTER TABLE `film_baru`
  MODIFY `id_genre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `genre`
--
ALTER TABLE `genre`
  MODIFY `id_genre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
